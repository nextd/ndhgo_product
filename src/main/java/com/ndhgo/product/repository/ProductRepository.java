package com.ndhgo.product.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ndhgo.product.document.Product;

@Repository
public interface ProductRepository extends MongoRepository<Product, String>{

	//List<Product> findAllByStoreId(Long storeId);

}
