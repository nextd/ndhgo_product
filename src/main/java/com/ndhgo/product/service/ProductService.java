package com.ndhgo.product.service;

import java.util.List;

import com.ndhgo.product.document.Product;

public interface ProductService {

	List<Product> getAllProductByStoreId(Long storeId);

	Product findByProductId(String productId);

	Product addNewProduct(Long storeId, Product product);

	Product getProductById(String productId);

}
