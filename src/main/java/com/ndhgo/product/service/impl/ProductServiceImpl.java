package com.ndhgo.product.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import com.ndhgo.product.document.Product;
import com.ndhgo.product.service.ProductService;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "PRODUCT_SERVICE")
@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public List<Product> getAllProductByStoreId(Long storeId) {
		log.info("/* Request came into product service for the store id : {}", storeId);
	
		return null;//productRepository.findAllByStoreId(storeId);
	}

	@Override
	public Product findByProductId(String productId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Product addNewProduct(Long storeId, Product product) {
		
		log.info("/* Request came into product service for the product : {}", product);
		
		mongoTemplate.save(product);
		
		return product;
	}

	@Override
	public Product getProductById(String productId) {
		

		return mongoTemplate.findById(productId, Product.class);
	}


}
