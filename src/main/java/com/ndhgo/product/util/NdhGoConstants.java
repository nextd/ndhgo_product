/**
 * Copyright (c) 2021, Senrysa Technologies All Rights Reserved.
 *
 */
package com.ndhgo.product.util;

/**
 * @author Akash chakraborty
 *
 */
public class NdhGoConstants {

	public static final String COLON = ":";
	public static final String COMMA = ",";
	public static final String COMMA_SEPERATOR = ", ";
	public static final String HYPHEN_SEPERATOR = " - ";

	public static final String BLANK_QUOTE = "";
	public static final String BLANK_SPACE = " ";

	public static final String YES = "Yes";
	public static final String NO = "No";

	public static final String TRUE = "true";
	public static final String FALSE = "false";

	public static final String ZERO = "0";
	public static final String ONE = "1";

	public static final String SUCCESS = "Success";
	public static final String FAILURE = "Failure";

	public static final String NDH_SDWFAX_CLIENT_ID = "nextdoorhub001";

	public static final String HEADER_AUTHORIZATION = "Authorization";
	public static final String HEADER_X_API_KEY = "x-api-key";
	public static final String HEADER_X_API_SECRET = "x-api-secret";
	public static final String HEADER_X_API_VERSION = "x-api-version";

	public static final String SYSTEM_TIMESTAMP_FORMAT = "dd MMM yyyy HH:mm:ss z";

	public static final String SDWFAX_TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS"; /* 2021-07-23T07:46:12.997506Z */
	public static final String SDWFAX_DATE_FORMAT = "yyyy-MM-dd"; /* 2021-07-23 */

	public static final String USER_SYSTEM = "SYSTEM";
	public static final String USER_SELLER = "SELLER";
	public static final String USER_END_CUSTOMER = "CUSTOMER";

	public static final String STORE_ID = "storeId";
	public static final String USER_ID = "userId";
	public static final String USER_NAME = "userName";
	public static final String USER_ROLE = "userRole";

	public static final String ROLE_ADMIN = "Admin";
	public static final String ROLE_MERCHANT = "MERCHANT";
	public static final String ROLE_MERCHANT_ADMIN = "MERCHANT-ADMIN";
	public static final String ROLE_HUB = "HUB";
	public static final String ROLE_HUB_ADMIN = "HUB-ADMIN";
	public static final String ROLE_SELLER = "SELLER";
	public static final String ROLE_STORE_ADMIN = "STORE-ADMIN";
	
	public static final String INVENTORY = "INVENTORY";
	public static final String VARIANT = "VARIANT";
	public static final String PRODUCT = "PRODUCT";

}
