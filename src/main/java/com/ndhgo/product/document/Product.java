package com.ndhgo.product.document;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Document(collection = "products")
public class Product {
	
	@Id
	@Field("_id")
	private String id;
	
	@JsonProperty("product_id")
	@Field("product_id")
	private Long productId;
	
	@JsonProperty("product_type")
	@Field("product_type")
	private String productType;
	
	private Boolean enabled;
	
	@JsonProperty("base_image")
	@Field("base_image")
	private String baseImage;
	
	private String slug;
	
	@JsonProperty("in_stock")
	@Field("in_stock")
	private Boolean inStock;
	
	@NotBlank
	@NotBlank
	@Size(min = 2 , max = 100 , message = "Not in size range 2 to 100")
	private String title;
	
	@JsonProperty("translated_title")
	@Field("translated_title")
	private TranslatedTitle translatedTitle;
	
	private String description;
	
	@JsonProperty("translated_description")
	@Field("translated_description")
	private TranslatedDescription translatedDescription;
	
	private String brand;
	
	@JsonProperty("display_tag")
	@Field("display_tag")
	private List<String> displayTag;
	
	@JsonProperty("search_tags")
	@Field("search_tags")
	private String searchTags;
	
	@JsonProperty("metafields")
	@Field("metafields")
	private Metafields metaFields;
	
	@JsonProperty("variant_groups")
	@Field("variant_groups")
	private VariantGroups variantGroups;
	
	@JsonProperty("add_group")
	@Field("add_group")
	private List<AddGroup> addGroup;
	
	private List<Attribute> attributes;
	
	private Media media;
	
	private List<Availability> availability;
	
	@JsonProperty("fulfillment_modes")
	@Field("fulfillment_modes")
	private List<String> fulfillmentModes;
	
	@JsonProperty("payment_mode")
	@Field("payment_mode")
	private List<String> paymentMode;
	
	@JsonProperty("product_return")
	@Field("product_return")
	private ProductReturn productReturn;
	
	@JsonProperty("tax_inclusive")
	@Field("tax_inclusive")
	private Boolean taxInclusive;
	
	@JsonProperty("tax_value")
	@Field("tax_value")
	private Double taxValue;
	
	private List<Tax> taxes;
	
	private List<Charge> charges;
	
	private List<Review> reviews;
	
	@JsonProperty("base_category_id")
	@Field("base_category_id")
	private String baseCategoryId;
	
	@JsonProperty("base_category_name")
	@Field("base_category_name")
	private String baseCategoryName;
	
	private List<Catgory> catgories;
	
	@JsonProperty("whole_sale_prices")
	@Field("whole_sale_prices")
	private List<WholesalePrice> wholeSalePrices;
	
	@CreatedDate
	@JsonProperty("create_time_stamp")
	@Field("createTimeStamp")
	private Date createTimeStamp;
	
	@LastModifiedDate
	@JsonProperty("update_time_stamp")
	@Field("update_time_stamp")
	private Date updateTimeStamp;
	
	@JsonProperty("product_class_id")
	@Field("product_class_id")
	private Integer productClassId;

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class TranslatedTitle {
		private String en;
		private String bn;
		private String hn;
	}

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class TranslatedDescription {
		private String en;
		private String bn;
	}

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class Metafields {
		
		private String url;
		
		@JsonProperty("meta_title")
		@Field("meta_title")
		private String metaTitle;
		
		@JsonProperty("meta_description")
		@Field("meta_description")
		private String metaDescription;
		
		@JsonProperty("meta_keywords")
		@Field("meta_keywords")
		private List<String> metaKeywords;
	}

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class Dimension {
		private Double lenght;
		private Double width;
		private Double height;
	}

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class AdvanceInventory {
		
		@JsonProperty("inventory_id")
		@Field("inventory_id")
		private Long inventoryId;
		
		@JsonProperty("inventory_quantity")
		@Field("inventory_quantity")
		private Double inventoryQuantity;
		
		private String sku;
		
		private String price;
		
		@JsonProperty("global_price")
		@Field("global_price")
		private Map<String, Double> globalPrice;
		
		@JsonProperty("compare_at_price")
		@Field("compare_at_price")
		private String compareAtPrice;
		
		@JsonProperty("global_compare_at_price")
		@Field("global_compare_at_price")
		private Map<String, Double> globalCompareAtPrice;
		
		private String barcode;
		
		@JsonProperty("default")
		@Field("default")
		private Boolean myDefault;
		
		private Boolean active;
	}

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class Attribute {
		
		@Id
		@Field("_id")
		private String id;
		
		private String name;
		
		private String value;
		
		@JsonProperty("is_filterable")
		@Field("is_filterable")
		private Boolean isFilterable;
	}


	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class Slots {
		
		@JsonProperty("start_time")
		@Field("start_time")
		private String startTime;
		
		@JsonProperty("end_time")
		@Field("end_time")
		private String endTime;
	}

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class Availability {
		
		@JsonProperty("ref_id")
		@Field("ref_id")
		private String refId;
		
		private List<String> day;
		
		private Slots slots;
	}

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class ProductReturn {
		
		@JsonProperty("is_returable")
		@Field("is_returable")
		private Boolean isReturable;
		
		@JsonProperty("is_replaceable")
		@Field("is_replaceable")
		private Boolean isReplaceable;
		
		@JsonProperty("return_period")
		@Field("return_period")
		private Integer returnPeriod;
		
		@JsonProperty("replacment_period")
		@Field("replacment_period")
		private Integer replacmentPeriod;
		
		@JsonProperty("return_note")
		@Field("return_note")
		private String returnNote;
		
		@JsonProperty("replacement_note")
		@Field("replacement_note")
		private String replacementNote;
	}

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class Tax {
		
		@JsonProperty("tax_id")
		@Field("tax_id")
		private Integer taxId;
		
		@JsonProperty("tax_type")
		@Field("tax_type")
		private String taxType;
		
		@JsonProperty("tax_value")
		@Field("tax_value")
		private Double taxValue;
	}

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class Charge {
		
		@Id
		@Field("_id")
		private String id;
		
		private String description;
		
		private String title;
		
		@JsonProperty("fulfillment_modes")
		@Field("fulfillment_modes")
		private List<String> fulfillmentModes;
		
		private String type;
		
		private Integer value;
		
		@JsonProperty("ref_id")
		@Field("ref_id")
		private String refId;
	}

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class Catgory {
		
		@JsonProperty("category_id")
		@Field("category_id")
		private String categoryId;
		
		@JsonProperty("category_name")
		@Field("category_name")
		private String categoryName;
	}

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class WholesalePrice {
		
		@JsonProperty("product_id")
		@Field("product_id")
		private String productId;
		
		@JsonProperty("from_quantity")
		@Field("from_quantity")
		private Integer fromQuantity;
		
		@JsonProperty("upto_quantity")
		@Field("upto_quantity")
		private Integer uptoQuantity;
		
		private String price;
		
		@JsonProperty("global_price")
		@Field("global_price")
		private Map<String, Double> globalPrice;
	}
}