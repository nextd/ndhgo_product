package com.ndhgo.product.document;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Document
public class Review {
	
	@Id
	@Field("_id")
	private String id;
	
	@JsonProperty("product_id")
	@Field("product_id")
	private String productId;
	
	private String title;
	
	private String text;
	
	private String status;
	
	private Integer rating;
	
	private String email;
	
	private String name;
	
	private String mobile;
	
	@JsonProperty("user_id")
	@Field("user_id")
	private Integer userId;
	
	@JsonProperty("date_reviewed")
	@Field("date_reviewed")
	private Date dateReviewed;
	
	@JsonProperty("date_created")
	@Field("date_created")
	private String dateCreated;
	
	@JsonProperty("date_modified")
	@Field("date_modified")
	private Date dateModified;
}
