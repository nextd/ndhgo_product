package com.ndhgo.product.document;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Document(collection = "product_media")
public class Media {
	
	@Id
	@Field("_id")
	private String id;
	
	@JsonProperty("product_id")
	@Field("product_id")
	private String productId;
	
	private List<Asset> assets;
	
	private List<Attachment> attachments;
	
	@JsonProperty("product_cms_url")
	@Field("product_cms_url")
	private String productCmsUrl;
	
	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class Asset {
		
		@Id
		@Field("_id")
		private String id;
		
		@JsonProperty("base_asset")
		@Field("base_asset")
		private Boolean baseAsset;
		
		@JsonProperty("asset_type")
		@Field("asset_type")
		private String assetType;
		
		private Integer position;
		
		private String alt;
		
		private String url;
		
		private String processor;
		
		@JsonProperty("image_url_160px")
		@Field("image_url_160px")
		private String imageUrl160px;
		
		@JsonProperty("image_url_400px")
		@Field("image_url_400px")
		private String imageUrl400px;
		
		@JsonProperty("image_url_800px")
		@Field("image_url_800px")
		private String imageUrl800px;
		
		@JsonProperty("image_url_1500px")
		@Field("image_url_1500px")
		private String imageUrl1500px;
		
		@JsonProperty("variant_ids")
		@Field("variant_ids")
		private List<Long> variantIds;
	}
	
	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class Attachment {
		
		@JsonProperty("file_type")
		@Field("file_type")
		private String fileType;
		
		@JsonProperty("file_description")
		@Field("file_description")
		private String fileDescription;
		
		private String alt;
		
		@JsonProperty("file_url")
		@Field("file_url")
		private String fileUrl;
	}
}