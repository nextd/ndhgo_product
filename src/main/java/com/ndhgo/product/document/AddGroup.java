package com.ndhgo.product.document;

import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Document
public class AddGroup {
	
	@Id
	@Field("_id")
	private String id;
	
	@JsonProperty("product_id")
	@Field("product_id")
	private String productId;
	
	private String title;
	
	@JsonProperty("minimum_needed")
	@Field("minimum_needed")
	private Integer minimumNeeded;
	
	@JsonProperty("maximum_allowed")
	@Field("maximum_allowed")
	private Integer maximumAllowed;
	
	private List<Addon> addons;
	
	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class Addon {
		
		@Id
		@Field("_id")
		private String id;
		
		@JsonProperty("inventory_id")
		@Field("inventory_id")
		private Integer inventoryId;
		
		private String price;
		
		private String title;
		
		@JsonProperty("in_stock")
		@Field("in_stock")
		private Boolean inStock;
		
		@JsonProperty("global_price")
		@Field("global_price")
		private Map<String, Double> globalPrice;
		
		@JsonProperty("compare_at_price")
		@Field("compare_at_price")
		private String compareAtPrice;
		
		@JsonProperty("global_compare_at_price")
		@Field("global_compare_at_price")
		private Map<String, Double> globalCompareAtPrice;
		
		@JsonProperty("food_type")
		@Field("food_type")
		private Integer foodType;
		
		@JsonProperty("ref_id")
		@Field("ref_id")
		private String refId;
	}
}
