package com.ndhgo.product.document;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ndhgo.product.document.Product.AdvanceInventory;
import com.ndhgo.product.document.Product.Dimension;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Document
public class VariantGroups {
	
	@Id
	@Field("_id")
	private String id;
	
	@JsonProperty("product_id")
	@Field("product_id")
	private String productId;
	
	private String type;
	
	private List<Variant> variants;
	
	private List<Option> options;
	
	
	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class Variant {
		
		@Id
		@Field("_id")
		private String id;
		
		@JsonProperty("product_id")
		@Field("product_id")
		private Long productId;
		
		private String sku;
		
		private String title;
		
		private String price;
		
		@JsonProperty("global_price")
		@Field("global_price")
		private Map<String, Double> globalPrice;
		
		@JsonProperty("compare_at_price")
		@Field("compare_at_price")
		private String compareAtPrice;
		
		@JsonProperty("global_compare_at_price")
		@Field("global_compare_at_price")
		private Map<String, Double> globalCompareAtPrice;
		
		private Integer position;
		
		private String option1;
		
		private String option2;
		
		private String option3;
		
		@CreatedDate
		@JsonProperty("created_at")
		@Field("created_at")
		private Date createdAt;
		
		@LastModifiedDate
		@JsonProperty("updated_at")
		@Field("updated_at")
		private Date updatedAt;
		
		private Boolean taxable;
		
		@JsonProperty("display_unit")
		@Field("display_unit")
		private String displayUnit;
		
		@JsonProperty("unit_value")
		@Field("unit_value")
		private Integer unitValue;
		
		@JsonProperty("base_unit")
		@Field("base_unit")
		private String baseUnit;
		
		@JsonProperty("dead_weight")
		@Field("dead_weight")
		private Integer deadWeight;
		
		@JsonProperty("weight_unight")
		@Field("weight_unight")
		private String weightUnight;
		
		@JsonProperty("dimension_unit")
		@Field("dimension_unit")
		private String dimensionUnit;
		
		private Dimension dimension;
		
		@JsonProperty("minimum_cart_item")
		@Field("minimum_cart_item")
		private Integer minimumCartItem;
		
		@JsonProperty("allow_cart_fraction")
		@Field("allow_cart_fraction")
		private Boolean allowCartFraction;
		
		@JsonProperty("cart_incremental_value")
		@Field("cart_incremental_value")
		private Integer cartIncrementalValue;
		
		@JsonProperty("inventory_policy")
		@Field("inventory_policy")
		private String inventoryPolicy;
		
		@JsonProperty("inventory_management")
		@Field("inventory_management")
		private String inventoryManagement;
		
		private String barcode;
		
		@JsonProperty("inventory_item_id")
		@Field("inventory_item_id")
		private Integer inventoryItemId;
		
		@JsonProperty("inventory_quantity")
		@Field("inventory_quantity")
		private Integer inventoryQuantity;
		
		@JsonProperty("requires_shipping")
		@Field("requires_shipping")
		private Boolean requiresShipping;
		
		@JsonProperty("advance_inventory")
		@Field("advance_inventory")
		private List<AdvanceInventory> advanceInventory;
	}
	
	
	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	@Builder
	@Document
	public static class Option {
		
		private String name;
		
		private Integer position;
		
		private List<String> values;
	}
	
	
	
}
