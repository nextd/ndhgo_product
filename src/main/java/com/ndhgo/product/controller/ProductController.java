package com.ndhgo.product.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ndhgo.product.document.Product;
import com.ndhgo.product.service.ProductService;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "PRODUCT_CONTROLLER")
@RequestMapping("/v2/")
@RestController
public class ProductController {
	
	@Autowired
	private ProductService productService;

	@GetMapping(value = "/get-all-product/{storeId}")
	public List<Product> getAllUsers(@PathVariable("storeId") Long storeId) {
		log.info("Getting all product");
		
		return productService.getAllProductByStoreId(storeId);
	}
	
	@GetMapping(value = "/{storeId}/product/{productId}")
	public Product getAllUsers(@PathVariable("storeId") Long storeId, @PathVariable("productId") String productId) {
		log.info("Getting product by product object Id: {}", productId);
		
		return productService.getProductById(productId);
	}
	
	@PostMapping(value = "/{storeId}/product")
	public Product addNewProduct(@PathVariable("storeId") Long storeId, @RequestBody @Valid Product product, BindingResult bindingResult) throws Exception {
		log.info("Create new product for the store ID: {}  and product json: {}",storeId ,product);
		
		if (bindingResult.hasErrors()) {
		    throw new Exception("Validation Error");
		  }
		
		productService.addNewProduct(storeId, product);
		
		return null;
	}
	
}
